import express from 'express';
import configViewEngine from './configs/viewEngine';
import initWebRoute from './route/web';
import initAPIRoute from './route/api';
import cron from 'node-cron';
import { saveDataShopee } from './controller/ShopeeController';
import { formattingJson } from './utils';
import sendoAPIRoute from './route/sendoApi';
import tiktokAPIRoute from './route/tiktokApi';
import lazadaAPIRoute from './route/lazadaApi';
import tikiAPIRoute from './route/tikiApi';

// import connection from './configs/connectDB';

require('dotenv').config();
var morgan = require('morgan');

const app = express();
const port = process.env.PORT || 8081;

app.use((req, res, next) => {
  //check => return res.send()
  console.log('>>> run into my middleware');
  console.log(req.method);
  next();
});

app.use(formattingJson);

app.use(morgan('combined'));

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// setup view engine
configViewEngine(app);

//init web route
initWebRoute(app);

// init api route
initAPIRoute(app);

sendoAPIRoute(app);
tiktokAPIRoute(app);
lazadaAPIRoute(app);
tikiAPIRoute(app);

cron.schedule('0 0 * * *', () => {
  saveDataShopee();
});

//handle 404 not found
app.use((req, res) => {
  return res.render('404.ejs');
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});

export default app;
