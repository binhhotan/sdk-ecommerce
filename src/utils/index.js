export const formattingJson = (req, res, next) => {
  res.success = data => {
    return res.status(200).json({ success: true, data });
  };

  res.error = (message, statusCode = 500) => {
    return res.status(statusCode).json({ success: false, error: message });
  };

  next();
};
