// const getCommonParameters = path => {
//   const currentTime = new Date();
//   const timest = Math.floor(currentTime.getTime() / 1000);
//   const access_token = localStorage.getItem('tiktok-access-token');

//   const baseString2 = `${process.env.APP_TIKTOK_SECRET}${path}app_key${process.env.APP_TIKTOK_KEY}timestamp${timest}${process.env.APP_TIKTOK_SECRET}`;
//   const sign = createHmac('sha256', process.env.APP_TIKTOK_SECRET)
//     .update(baseString2)
//     .digest('hex');

//   return {
//     app_key: process.env.APP_TIKTOK_KEY,
//     timestamp: timest,
//     access_token: access_token,
//     sign,
//   };
// };

const getUrlAuth = async (req, res) => {
  const url = process.env.APP_LAZADA_URL;
  const params = {
    app_key: process.env.APP_LAZADA_KEY,
    redirect_uri:
      'https://sdk-ecommerce.vercel.app/api/v1/lazada/auth/get-token',
  };

  return res.success(
    `${url}/oauth/authorize?client_id=${params.app_key}&redirect_uri=${params.redirect_uri}&response_type=code`
  );
};

module.exports = {
  getUrlAuth,
};
