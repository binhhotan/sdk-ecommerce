// import axios from 'axios';
import axios from 'axios/dist/node/axios.cjs';
// const LocalStorage = require('node-localstorage').LocalStorage;
// const localStorage = new LocalStorage('./localStorage');

const getToken = async (req, res) => {
  const path = '/login';
  const url = process.env.APP_SENDO_URL + path;

  const param = {
    shop_key: process.env.APP_SENDO_SHOP,
    secret_key: process.env.APP_SENDO_SECRET,
  };

  const responseOrder = await axios.post(url, param, {
    headers: {
      'Content-Type': 'application/json',
    },
  });

  const data = responseOrder.data;
  localStorage.setItem('sendo-access-token', responseOrder.data.result.token);

  return res.success(data);
};
const getOrders = async (req, res) => {
  const path = '/api/partner/salesorder/search';
  const url = process.env.APP_SENDO_URL + path;
  const token = localStorage.getItem('sendo-access-token');

  const responseOrder = await axios.post(url, req.body, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });

  const data = responseOrder.data;

  return res.success(data);
};
const getCancelReasonCollection = async (req, res) => {
  const path = '/api/partner/salesorder/reason-collection';
  const url = process.env.APP_SENDO_URL + path;
  const token = localStorage.getItem('sendo-access-token');

  const responseOrder = await axios.get(url, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });

  const data = responseOrder.data;

  return res.success(data);
};
const updateOrderStatus = async (req, res) => {
  const path = '/api/partner/salesorder';
  const url = process.env.APP_SENDO_URL + path;
  const token = localStorage.getItem('sendo-access-token');

  const responseOrder = await axios.put(url, req.body, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });

  const data = responseOrder.data;

  return res.success(data);
};
const getProductList = async (req, res) => {
  const path = '/api/partner/product/search';
  const url = process.env.APP_SENDO_URL + path;
  const token = localStorage.getItem('sendo-access-token');

  const responseOrder = await axios.post(url, req.body, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });

  const data = responseOrder.data;

  return res.success(data);
};
const getCategory = async (req, res) => {
  const path = `/api/partner/category/${req.params.id}`;
  const url = process.env.APP_SENDO_URL + path;
  const token = localStorage.getItem('sendo-access-token');

  const responseOrder = await axios.get(url, {
    params: req.query,
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });

  const data = responseOrder.data;

  return res.success(data);
};
const getCategoryAttribute = async (req, res) => {
  const path = `/api/partner/category/attribute/${req.params.id}`;
  const url = process.env.APP_SENDO_URL + path;
  const token = localStorage.getItem('sendo-access-token');

  const responseOrder = await axios.get(url, {
    params: req.query,
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });

  const data = responseOrder.data;

  return res.success(data);
};
const getBrand = async (req, res) => {
  const path = `/api/partner/category/brand/${req.params.id}`;
  const url = process.env.APP_SENDO_URL + path;
  const token = localStorage.getItem('sendo-access-token');

  const responseOrder = await axios.get(url, {
    params: req.query,
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });

  const data = responseOrder.data;

  return res.success(data);
};
const createProduct = async (req, res) => {
  const path = '/api/partner/product';
  const url = process.env.APP_SENDO_URL + path;
  const token = localStorage.getItem('sendo-access-token');

  const responseOrder = await axios.post(url, req.body, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });

  const data = responseOrder.data;

  return res.success(data);
};
const createMultiProduct = async (req, res) => {
  const path = '/api/partner/product/list';
  const url = process.env.APP_SENDO_URL + path;
  const token = localStorage.getItem('sendo-access-token');

  const responseOrder = await axios.post(url, req.body, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });

  const data = responseOrder.data;

  return res.success(data);
};
const getProductDetail = async (req, res) => {
  const path = '/api/partner/product';
  const url = process.env.APP_SENDO_URL + path;
  const token = localStorage.getItem('sendo-access-token');

  const responseOrder = await axios.get(url, {
    params: req.query,
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });

  const data = responseOrder.data;

  return res.success(data);
};

module.exports = {
  getToken,
  getOrders,
  getCancelReasonCollection,
  updateOrderStatus,
  getProductList,
  createProduct,
  createMultiProduct,
  getCategory,
  getCategoryAttribute,
  getBrand,
  getProductDetail,
};
