import { createHmac } from 'crypto';
// import axios from 'axios';
import axios from 'axios/dist/node/axios.cjs';
import pool from '../configs/connectDB';

// const LocalStorage = require('node-localstorage').LocalStorage;
// const localStorage = new LocalStorage('./localStorage');

const getCommonParameters = path => {
  const currentTime = new Date();
  const timest = Math.floor(currentTime.getTime() / 1000);
  const access_token = localStorage.getItem('access-token');
  const shop_id = localStorage.getItem('shop-id');

  const baseString2 = `${process.env.APP_PARTNER_ID}${path}${timest}${access_token}${shop_id}`;
  const sign = createHmac('sha256', process.env.APP_PARTNER_KEY)
    .update(baseString2)
    .digest('hex');

  return {
    partner_id: Number(process.env.APP_PARTNER_ID),
    timestamp: timest,
    access_token,
    shop_id: Number(shop_id),
    sign,
  };
};

const getAuthPage = async (req, res) => {
  const path = '/api/v2/shop/auth_partner';
  const redirectUrl = 'http://localhost:8081/get-token';

  const currentTime = new Date();
  const timest = Math.floor(currentTime.getTime() / 1000);

  const baseString = `${process.env.APP_PARTNER_ID}${path}${timest}`;
  const sign = createHmac('sha256', process.env.APP_PARTNER_KEY)
    .update(baseString)
    .digest('hex');

  const url = `${process.env.APP_AUTH_URL}${path}?partner_id=${process.env.APP_PARTNER_ID}&redirect=${redirectUrl}&timestamp=${timest}&sign=${sign}`;

  return res.render('shopeeAuth.ejs', {
    link: url,
  });
};

const getAuthPageApi = async (req, res) => {
  const path = '/api/v2/shop/auth_partner';
  const redirectUrl = 'http://localhost:8081/get-token';

  const currentTime = new Date();
  const timest = Math.floor(currentTime.getTime() / 1000);

  const baseString = `${process.env.APP_PARTNER_ID}${path}${timest}`;
  const sign = createHmac('sha256', process.env.APP_PARTNER_KEY)
    .update(baseString)
    .digest('hex');

  const url = `${process.env.APP_AUTH_URL}${path}?partner_id=${process.env.APP_PARTNER_ID}&redirect=${redirectUrl}&timestamp=${timest}&sign=${sign}`;

  return res.status(200).json({
    message: 'success',
    data: {
      url,
    },
  });
};

let getAccessToken = async (req, res) => {
  const code = req.query.code;
  const shop_id = req.query.shop_id;

  const path = '/api/v2/auth/token/get';
  const currentTime = new Date();
  const timest = Math.floor(currentTime.getTime() / 1000);

  const baseString = `${process.env.APP_PARTNER_ID}${path}${timest}`;
  const sign = createHmac('sha256', process.env.APP_PARTNER_KEY)
    .update(baseString)
    .digest('hex');

  const url = `${process.env.APP_AUTH_URL}${path}?partner_id=${process.env.APP_PARTNER_ID}&timestamp=${timest}&sign=${sign}`;

  const requestData = {
    code,
    shop_id: Number(shop_id),
    partner_id: Number(process.env.APP_PARTNER_ID),
  };

  const response = await axios.post(url, requestData, {
    headers: {
      'Content-Type': 'application/json',
    },
  });

  const responseData = response.data;

  localStorage.setItem('access-token', responseData?.access_token);
  localStorage.setItem('refresh-token', responseData?.refresh_token);
  localStorage.setItem('request-id', responseData?.request_id);
  localStorage.setItem('code', code);
  localStorage.setItem('shop-id', shop_id);

  // return res.redirect('/dashboard');
  return res.status(200).json({
    code: 200,
    message: 'success',
    data: {
      code,
      shop_id,
      access_token: responseData?.access_token,
      refresh_token: responseData?.refresh_token,
    },
  });
};

const saveDataShopee = async (req, res) => {
  const currentTime = new Date();
  const timest = Math.floor(currentTime.getTime() / 1000);
  const access_token = localStorage.getItem('access-token');
  const shop_id = localStorage.getItem('shop-id');
  // const code = localStorage.getItem('code');

  const path2 = '/api/v2/order/get_order_list';
  const url2 = process.env.APP_AUTH_URL + path2;

  const baseString2 = `${process.env.APP_PARTNER_ID}${path2}${timest}${access_token}${shop_id}`;
  const sign2 = createHmac('sha256', process.env.APP_PARTNER_KEY)
    .update(baseString2)
    .digest('hex');

  const requestData = {
    partner_id: Number(process.env.APP_PARTNER_ID),
    timestamp: timest,
    access_token,
    shop_id: Number(shop_id),
    sign: sign2,
    page_size: 20,
    time_from: timest - 60,
    time_to: timest,
  };

  const responseOrderCreate = await axios.get(url2, {
    params: { ...requestData, time_range_field: 'create_time' },
  });
  const responseDataOrderCreate = responseOrderCreate.data;

  const responseOrderUpdate = await axios.get(url2, {
    params: { ...requestData, time_range_field: 'update_time' },
  });
  const responseDataOrderUpdate = responseOrderUpdate.data;

  for (const data of responseDataOrderCreate) {
    await pool.execute(
      'insert into order(order_sn, order_status) values (?, ?)',
      [data.order_sn, data.order_status]
    );
  }

  for (const data of responseDataOrderUpdate) {
    await pool.execute('update order  order_status = ?  where order_sn = ?', [
      data.order_status,
      data.order_sn,
    ]);
  }
};

const getOrders = async (req, res) => {
  const path = '/api/v2/order/get_order_list';
  const url = process.env.APP_AUTH_URL + path;

  const request = getCommonParameters(path);

  const requestData = {
    ...request,
    ...req.query,
  };

  const responseOrder = await axios.get(url, {
    params: requestData,
  });
  const data = responseOrder.data;

  return res.success(data);

  // return res.status(200).json({
  //   message: 'success',
  //   data,
  // });
};

const pageListOrders = async (req, res) => {
  const [rows, fields] = await pool.execute('SELECT * FROM orders');

  return res.render('dashboard.ejs', {
    data: JSON.stringify(rows),
  });
};

const getInfoShop = async (req, res) => {
  const path = '/api/v2/shop/get_shop_info';
  const url = process.env.APP_AUTH_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.get(url, {
    params: requestData,
  });
  const data = responseOrder.data;

  return res.status(200).json({
    message: 'success',
    data,
  });
};
const getProfile = async (req, res) => {
  const path = '/api/v2/shop/get_profile';
  const url = process.env.APP_AUTH_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.get(url, {
    params: requestData,
  });
  const data = responseOrder.data;

  return res.status(200).json({
    message: 'success',
    data,
  });
};
const getCategory = async (req, res) => {
  const path = '/api/v2/product/get_category';
  const url = process.env.APP_AUTH_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.get(url, {
    params: requestData,
  });
  const data = responseOrder.data;

  return res.status(200).json({
    message: 'success',
    data,
  });
};
const getBrand = async (req, res) => {
  const path = '/api/v2/product/get_brand_list';
  const url = process.env.APP_AUTH_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.get(url, {
    params: requestData,
  });
  const data = responseOrder.data;

  return res.status(200).json({
    message: 'success',
    data,
  });
};
const getLogisticChanel = async (req, res) => {
  const path = '/api/v2/logistics/get_channel_list';
  const url = process.env.APP_AUTH_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.get(url, {
    params: requestData,
  });
  const data = responseOrder.data;

  return res.status(200).json({
    message: 'success',
    data,
  });
};

const updateProfile = async (req, res) => {
  const path = '/api/v2/shop/update_profile';
  const url = process.env.APP_AUTH_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.post(url, req.body, {
    params: requestData,
  });

  const data = responseOrder.data;

  return res.status(200).json({
    message: 'success',
    data,
  });
};
const getListItem = async (req, res) => {
  const path = '/api/v2/product/get_item_list';
  const url = process.env.APP_AUTH_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.get(url, {
    params: {
      ...requestData,
      ...req.query,
    },
  });

  const data = responseOrder.data;

  return res.status(200).json({
    message: 'success',
    data,
  });
};
const getItemBaseInfo = async (req, res) => {
  const path = '/api/v2/product/get_item_base_info';
  const url = process.env.APP_AUTH_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.get(url, {
    params: {
      ...requestData,
      ...req.query,
    },
  });

  const data = responseOrder.data;

  return res.status(200).json({
    message: 'success',
    data,
  });
};
const getItemExtraInfo = async (req, res) => {
  const path = '/api/v2/product/get_item_extra_info';
  const url = process.env.APP_AUTH_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.get(url, {
    params: {
      ...requestData,
      ...req.query,
    },
  });

  const data = responseOrder.data;

  return res.status(200).json({
    message: 'success',
    data,
  });
};

const addItem = async (req, res) => {
  const path = '/api/v2/product/add_item';
  const url = process.env.APP_AUTH_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.post(url, req.body, {
    params: requestData,
  });

  const data = responseOrder.data;

  return res.status(200).json({
    message: 'success',
    data,
  });
};
const updateItem = async (req, res) => {
  const path = '/api/v2/product/update_item';
  const url = process.env.APP_AUTH_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.post(url, req.body, {
    params: requestData,
  });

  const data = responseOrder.data;

  return res.status(200).json({
    message: 'success',
    data,
  });
};
const deleteItem = async (req, res) => {
  const path = '/api/v2/product/delete_item';
  const url = process.env.APP_AUTH_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.post(url, req.body, {
    params: requestData,
  });

  const data = responseOrder.data;

  return res.status(200).json({
    message: 'success',
    data,
  });
};

const getOrderDetail = async (req, res) => {
  const path = '/api/v2/order/get_order_detail';
  const url = process.env.APP_AUTH_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.get(url, {
    params: {
      ...requestData,
      ...req.query,
    },
  });

  const data = responseOrder.data;

  return res.status(200).json({
    message: 'success',
    data,
  });
};
const getWarehouseDetail = async (req, res) => {
  const path = '/api/v2/shop/get_warehouse_detail';
  const url = process.env.APP_AUTH_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.get(url, {
    params: requestData,
  });

  const data = responseOrder.data;

  return res.success(data);
};

const cancelOrder = async (req, res) => {
  const path = '/api/v2/order/cancel_order';
  const url = process.env.APP_AUTH_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.post(url, req.body, {
    params: requestData,
  });

  const data = responseOrder.data;

  return res.status(200).json({
    message: 'success',
    data,
  });
};

const uploadImage = async (req, res) => {
  const path = '/api/v2/media_space/upload_image';
  const url = process.env.APP_AUTH_URL + path;
  // const requestData = getCommonParameters(path);
  const currentTime = new Date();
  const timest = Math.floor(currentTime.getTime() / 1000);

  const baseString2 = `${process.env.APP_PARTNER_ID}${path}${timest}`;
  const sign = createHmac('sha256', process.env.APP_PARTNER_KEY)
    .update(baseString2)
    .digest('hex');

  const requestData = {
    partner_id: Number(process.env.APP_PARTNER_ID),
    timestamp: timest,
    sign,
  };

  const body = {
    image: req.files,
    scene: 'normal',
  };

  console.log(body);

  const responseOrder = await axios.post(url, body, {
    params: requestData,
  });

  const data = responseOrder.data;

  return res.status(200).json({
    message: 'success',
    data,
  });
};

module.exports = {
  getAuthPage,
  getAccessToken,
  saveDataShopee,
  getOrders,
  pageListOrders,
  getAuthPageApi,
  getInfoShop,
  getProfile,
  updateProfile,
  getListItem,
  getItemBaseInfo,
  getOrderDetail,
  cancelOrder,
  getItemExtraInfo,
  updateItem,
  uploadImage,
  deleteItem,
  addItem,
  getCategory,
  getBrand,
  getLogisticChanel,
  getWarehouseDetail,
};
