// import axios from 'axios';
import axios from 'axios/dist/node/axios.cjs';
import crypto from 'crypto';
// const LocalStorage = require('node-localstorage').LocalStorage;
// const localStorage = new LocalStorage('./localStorage');

const getCommonParameters = path => {
  const inputString = `${process.env.APP_TIKI_KEY}:${process.env.APP_TIKI_SECRET}`;

  const buffer = Buffer.from(inputString, 'utf-8');

  const base64EncodedString = buffer.toString('base64');

  return base64EncodedString;
};

const getUrlAuth = async (req, res) => {
  const path = '/auth';
  const url = process.env.APP_TIKI_AUTH + path;

  const params = {
    response_type: 'code',
    client_id: process.env.APP_TIKI_KEY,
    redirect_uri: 'http://localhost:8081/api/v1/tiki/auth/get-token',
    state: '12345678binhho',
    scope: 'offline product order',
  };

  return res.success(
    `${url}?response_type=${params.response_type}&client_id=${params.client_id}&redirect_uri=${params.redirect_uri}&state=${params.state}&scope=${params.scope}`
  );
};

const getToken = async (req, res) => {
  const path = '/token';
  const url = process.env.APP_TIKI_AUTH + path;

  const body = {
    grant_type: 'authorization_code',
    code: req.query.code,
    redirect_uri: 'http://localhost:8081/api/v1/tiki/auth/get-token',
    client_secret: process.env.APP_TIKI_SECRET,
    client_id: process.env.APP_TIKI_KEY,
  };

  const responseOrder = await axios.post(url, body, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
  });
  const data = responseOrder.data;

  localStorage.setItem('tiki-access-token', data.access_token);

  return res.success(data);
};

const getProducts = async (req, res) => {
  const path = '/products';
  const url = process.env.APP_TIKI_URL + path;
  const token = localStorage.getItem('tiki-access-token');
  const body = {
    client_secret: process.env.APP_TIKI_SECRET,
    client_id: process.env.APP_TIKI_KEY,
  };

  const responseOrder = await axios.get(url, {
    params: { ...body },
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      Authorization: `Basic ${token}`,
      Accept: 'application/json',
    },
  });
  const data = responseOrder.data;

  return res.success(data);
};

const getProfile = async (req, res) => {
  const path = '/v2/sellers/me';
  const url = process.env.APP_TIKI_URL + path;
  const token = localStorage.getItem('tiki-access-token');

  const responseOrder = await axios.get(url, {
    params: req.query,
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });
  const data = responseOrder.data;

  return res.success(data);
};
const getCategories = async (req, res) => {
  const path = '/v2/categories';
  const url = process.env.APP_TIKI_URL + path;
  const token = localStorage.getItem('tiki-access-token');

  const responseOrder = await axios.get(url, {
    params: req.query,
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });
  const data = responseOrder.data;

  return res.success(data);
};
const updateProduct = async (req, res) => {
  const path = '/v2.1/requests/updateProductInfo';
  const url = process.env.APP_TIKI_URL + path;
  const token = localStorage.getItem('tiki-access-token');
  console.log(token);

  const base64 = getCommonParameters();
  const responseOrder = await axios.post(url, req.body, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      Authorization: `Basic ${base64}`,
      Accept: 'application/json',
    },
  });
  const data = responseOrder.data;

  return res.success(data);
};

module.exports = {
  getUrlAuth,
  getToken,
  getProducts,
  getProfile,
  updateProduct,
  getCategories,
};
