import { createHmac } from 'crypto';
// import axios from 'axios';
import axios from 'axios/dist/node/axios.cjs';

// const LocalStorage = require('node-localstorage').LocalStorage;
// const localStorage = new LocalStorage('./localStorage');

const getCommonParameters = path => {
  const currentTime = new Date();
  const timest = Math.floor(currentTime.getTime() / 1000);
  const access_token = localStorage.getItem('tiktok-access-token');

  const baseString2 = `${process.env.APP_TIKTOK_SECRET}${path}app_key${process.env.APP_TIKTOK_KEY}timestamp${timest}${process.env.APP_TIKTOK_SECRET}`;
  const sign = createHmac('sha256', process.env.APP_TIKTOK_SECRET)
    .update(baseString2)
    .digest('hex');

  return {
    app_key: process.env.APP_TIKTOK_KEY,
    timestamp: timest,
    access_token: access_token,
    sign,
  };
};

const getUrlAuth = async (req, res) => {
  const url = process.env.APP_TIKTOK_AUTHORIZE;
  const params = {
    app_key: process.env.APP_TIKTOK_KEY,
    state: Math.floor(Math.random() * 999999),
  };

  return res.success(`${url}?app_key=${params.app_key}&state=${params.state}`);
};

const getToken = async (req, res) => {
  const tiktok_code = req.query.code;
  const path = '/api/v2/token/get';
  const url = process.env.APP_TIKTOK_AUTH + path;

  const params = {
    app_key: process.env.APP_TIKTOK_KEY,
    app_secret: process.env.APP_TIKTOK_SECRET,
    auth_code: tiktok_code,
    grant_type: 'authorized_code',
  };

  const responseData = await axios.get(url, { params });
  const data = responseData.data;

  localStorage.setItem('tiktok-access-token', data.data.access_token);
  localStorage.setItem('tiktok-refresh-token', data.data.refresh_token);
  localStorage.setItem('tiktok-open-id', data.data.open_id);

  return res.success(data);
};

const getShopInfo = async (req, res) => {
  const path = '/api/shop/get_authorized_shop';
  const url = process.env.APP_TIKTOK_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.get(url, {
    params: requestData,
  });
  const data = responseOrder.data;

  return res.success(data);
};

const getOrders = async (req, res) => {
  const path = '/api/orders/search';
  const url = process.env.APP_TIKTOK_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.post(url, req.query, {
    params: requestData,
  });
  const data = responseOrder.data;

  return res.success(data);
};

const getOrderDetail = async (req, res) => {
  const path = '/api/orders/detail/query';
  const url = process.env.APP_TIKTOK_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.post(url, req.body, {
    params: requestData,
  });
  const data = responseOrder.data;

  return res.success(data);
};

const createProduct = async (req, res) => {
  const path = '/api/products';
  const url = process.env.APP_TIKTOK_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.post(url, req.body, {
    params: requestData,
    headers: {
      'Content-Type': 'application/json',
    },
  });
  const data = responseOrder.data;

  return res.success(data);
};

const editProduct = async (req, res) => {
  const path = '/api/products';
  const url = process.env.APP_TIKTOK_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.put(url, {
    data: req.body,
    params: requestData,
  });
  const data = responseOrder.data;

  return res.success(data);
};

const deleteProduct = async (req, res) => {
  const path = '/api/products';
  const url = process.env.APP_TIKTOK_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.delete(url, {
    params: requestData,
    data: req.body,
  });
  const data = responseOrder.data;

  return res.success(data);
};
const getProductDetail = async (req, res) => {
  const path = '/api/products/details';
  const url = process.env.APP_TIKTOK_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.get(url, {
    params: {
      ...requestData,
      ...req.query,
    },
  });

  const data = responseOrder.data;

  return res.success(data);
};
const getProductList = async (req, res) => {
  const path = '/api/products/search';
  const url = process.env.APP_TIKTOK_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.post(url, req.query, {
    params: requestData,
  });
  const data = responseOrder.data;

  return res.success(data);
};
const getCategories = async (req, res) => {
  const path = '/api/products/categories';
  const url = process.env.APP_TIKTOK_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.get(url, {
    params: requestData,
  });
  const data = responseOrder.data;

  return res.success(data);
};
const getWarehouseList = async (req, res) => {
  const path = '/api/logistics/get_warehouse_list';
  const url = process.env.APP_TIKTOK_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.get(url, {
    params: requestData,
  });
  const data = responseOrder.data;

  return res.success(data);
};

const getAttributes = async (req, res) => {
  const path = '/api/products/attributes';
  const url = process.env.APP_TIKTOK_URL + path;
  const requestData = getCommonParameters(path);

  const responseOrder = await axios.get(url, {
    params: { ...requestData, ...req.query },
  });
  const data = responseOrder.data;

  return res.success(data);
};

module.exports = {
  getUrlAuth,
  getToken,
  getOrders,
  getOrderDetail,
  createProduct,
  editProduct,
  deleteProduct,
  getProductDetail,
  getProductList,
  getShopInfo,
  getCategories,
  getWarehouseList,
  getAttributes,
};
