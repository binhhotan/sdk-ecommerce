import express from 'express';
import TiktokController from '../controller/TiktokController';
let router = express.Router();

const tiktokAPIRoute = app => {
  router.get('/get-auth-url', TiktokController.getUrlAuth);
  router.get('/get-token', TiktokController.getToken);

  router.get('/get-shop-info', TiktokController.getShopInfo);

  router.get('/order/list', TiktokController.getOrders);
  router.post('/order/detail', TiktokController.getOrderDetail);

  router.post('/product/create', TiktokController.createProduct);
  router.post('/product/edit', TiktokController.editProduct);
  router.delete('/product/delete', TiktokController.deleteProduct);
  router.get('/product/detail', TiktokController.getProductDetail);
  router.get('/product/list', TiktokController.getProductList);
  router.get('/product/get-categories', TiktokController.getCategories);
  router.get('/product/get-warehouse', TiktokController.getWarehouseList);
  router.get('/product/get-attributes', TiktokController.getAttributes);

  return app.use('/api/v1/tiktok/', router);
};

export default tiktokAPIRoute;
