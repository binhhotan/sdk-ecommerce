import express from 'express';
import ShopeeController from '../controller/ShopeeController';
import multer from 'multer';
let router = express.Router();

let upload = multer({ dest: '' });

const initAPIRoute = app => {
  router.get('/auth/get-url', ShopeeController.getAuthPageApi);
  router.get('/auth/get-token', ShopeeController.getAccessToken);

  router.get('/shop/get-info', ShopeeController.getInfoShop);
  router.post('/shop/update-profile', ShopeeController.updateProfile);
  router.get('/shop/get-profile', ShopeeController.getProfile);
  router.get('/shop/get-warehouse-detail', ShopeeController.getWarehouseDetail);

  router.get('/product/list', ShopeeController.getListItem);
  router.get('/product/base-info', ShopeeController.getItemBaseInfo);
  router.get('/product/extra-info', ShopeeController.getItemExtraInfo);
  router.get('/product/get-category', ShopeeController.getCategory);
  router.get('/product/get-brand', ShopeeController.getBrand);
  router.get(
    '/product/get-list-logistic-chanel',
    ShopeeController.getLogisticChanel
  );

  router.post('/product/update-product', ShopeeController.updateItem);
  router.post('/product/delete-product', ShopeeController.deleteItem);
  router.post('/product/add-product', ShopeeController.addItem);

  router.post(
    '/upload-image',
    upload.single('image'),
    ShopeeController.uploadImage
  );

  router.get('/order/detail', ShopeeController.getOrderDetail);
  router.get('/order/list', ShopeeController.getOrders);
  router.post('/order/cancel-order', ShopeeController.cancelOrder);

  return app.use('/api/v1/shopee/', router);
};

export default initAPIRoute;
