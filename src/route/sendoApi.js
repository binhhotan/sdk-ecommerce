import express from 'express';
import SendoController from '../controller/SendoController';
let router = express.Router();

const sendoAPIRoute = app => {
  router.get('/auth/get-token', SendoController.getToken);

  router.post('/order/list', SendoController.getOrders);
  router.get(
    '/order/get-cancel-reason-collection',
    SendoController.getCancelReasonCollection
  );
  router.put('/order/update-order-status', SendoController.updateOrderStatus);

  router.post('/product/list', SendoController.getProductList);
  router.get('/product/category/:id', SendoController.getCategory);
  router.get(
    '/product/category-attribute/:id',
    SendoController.getCategoryAttribute
  );
  router.get('/product/brand/:id', SendoController.getBrand);
  router.post('/product/create', SendoController.createProduct); // update id = product_id
  router.post('/product/create-multi', SendoController.createMultiProduct); // update id = product_id
  router.get('/product/detail', SendoController.getProductDetail);

  return app.use('/api/v1/sendo/', router);
};

export default sendoAPIRoute;
