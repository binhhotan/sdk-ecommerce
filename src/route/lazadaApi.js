import express from 'express';
import LazadaController from '../controller/LazadaController';
let router = express.Router();

const lazadaAPIRoute = app => {
  router.get('/auth/get-url', LazadaController.getUrlAuth);

  return app.use('/api/v1/lazada/', router);
};

export default lazadaAPIRoute;
