import express from 'express';
import TikiController from '../controller/TikiController';
let router = express.Router();

const tikiAPIRoute = app => {
  router.get('/auth/get-url', TikiController.getUrlAuth);
  router.get('/auth/get-token', TikiController.getToken);

  // shop
  router.get('/shop/profile', TikiController.getProfile);

  //product
  router.get('/product/list', TikiController.getProducts);
  router.post('/product/update', TikiController.updateProduct);
  router.get('/product/categories', TikiController.getCategories);

  return app.use('/api/v1/tiki/', router);
};

export default tikiAPIRoute;
