import express from 'express';
import cron from 'node-cron';
import configViewEngine from './src/configs/viewEngine';
import initWebRoute from './src/route/web';
import initAPIRoute from './src/route/api';
import { saveDataShopee } from './src/controller/ShopeeController';
import { formattingJson } from './src/utils';
import sendoAPIRoute from './src/route/sendoApi';
import tiktokAPIRoute from './src/route/tiktokApi';
import lazadaAPIRoute from './src/route/lazadaApi';
import tikiAPIRoute from './src/route/tikiApi';

// import connection from './configs/connectDB';

require('dotenv').config();
var morgan = require('morgan');

const app = express();
const port = process.env.PORT || 8081;

app.use((req, res, next) => {
  //check => return res.send()
  console.log('>>> run into my middleware');
  console.log(req.method);
  next();
});

app.use(formattingJson);

app.use(morgan('combined'));

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// setup view engine
configViewEngine(app);

//init web route
initWebRoute(app);

// init api route
initAPIRoute(app);

sendoAPIRoute(app);
tiktokAPIRoute(app);
lazadaAPIRoute(app);
tikiAPIRoute(app);

cron.schedule('0 0 * * *', () => {
  saveDataShopee();
});

//handle 404 not found
app.use((req, res) => {
  return res.render('404.ejs');
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});

export default app;
